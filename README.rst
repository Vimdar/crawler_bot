CRAWLER BOT
===========================

Crawling Bot to collect all the info from list of different media.

Installation
------------
Clone or fork if needed.

Usage
-----
Use bots to crawl media sources.

**WARNING: This bot uses certain databases and has dependencies that are a matter of another setup.
You can change the bot but no backwards compatibility is guaranteed.

Tests
~~~~~

No tests so far #TODO

Repository
~~~~~~~~~~
----------
