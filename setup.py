import os
from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='crawler_bot',
    version='0.0.0',
    # namespace_packages=['bot_crawler'],
    # packages=[p for p in find_packages() if p.partition('.')[0] == 'bot_crawler'],
    # include_package_data=True,
    license='Mitaka',
    # install_requires=[],
    description=('Crawler to track list of media'),
    author='Dimitar Varbenov',
    author_email='dimitarvarbenov@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Scrapy',
        'Intended Audience :: Developers',
    ],
    zip_safe=True,
)
