from scrapy import cmdline


def run_spider():
    start_urls = 'http://bgradio.bg/article/index'
    allowed_domains = 'bgradio.bg'
    articles_xpath = "//article/a"
    menu_xpath = "(//ul[@class='submenu']/li/a)[1]"
    body_xpath = "//span[@itemprop='articleBody']//p"
    title_xpath = "//h1"
    source_id = 0
    mode = 'button'
    loadmore = 'button.nova-btn.btn.btn-lg.no-ajaxy.loadmore'
    loadmore_depth = 2
    comm = """scrapy crawl vspider -a start_urls=%s -a allowed_domains=%s
     -a title_xpath=%s -a menu_xpath=%s -a articles_xpath=%s -a loadmore=%s
      -a loadmore_depth=%s -a body_xpath=%s
       -a source_id=%s -a mode=%s""" % (start_urls, allowed_domains, title_xpath, menu_xpath, articles_xpath, loadmore, loadmore_depth, body_xpath, source_id, mode)
    cmdline.execute(comm.split())


if '__main__' == __name__:
    run_spider()

# scrapy crawl vspider -a start_urls=http://bgradio.bg/article/index -a allowed_domains=bgradio.bg -a title_xpath=//h1 -a menu_xpath=(//ul[@class='submenu']/li/a)[1] -a articles_xpath=//article/a -a loadmore=button -a loadmore_depth=3 -a body_xpath=//span[@itemprop='articleBody']//p -a source_id=3 -a mode=button
