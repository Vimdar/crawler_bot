# coding: utf8
import traceback
import datetime as dt, pytz
import re
import logging

import lxml.html
import psycopg2, psycopg2.extras, psycopg2.extensions

from scrapy.http import Request
from scrapy.exceptions import IgnoreRequest

from bot_crawler.items import Item as ScrapyItem, Article, Tweet, FacebookPost


psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)


BG_TZ = pytz.timezone('Europe/Sofia')


log = logging.getLogger(__name__)


class CrawlerDBStoreMiddleware(object):

    def __init__(self, crawler):

        # (source_id, item_id_in_source) → item_id
        self.dupfilter_cache = dict()

        self.stats = crawler.stats

        self.conn = psycopg2.connect(
            host='localhost',
            # port=crawler.settings.get('CRAWLERDB_STORE_DBPORT'),
            user='bot_master',
            password='local_pass1',
            database='bot',
            cursor_factory=psycopg2.extras.DictCursor,
        )
        self.conn.autocommit = True
        with self.conn.cursor() as cur:
            cur.execute("SET application_name = 'media_crawler:db_storage_module';")

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)

    def process_spider_output(self, response, result, spider):
        for req_or_item in result:
            if isinstance(req_or_item, ScrapyItem):
                try:
                    item = self.process_item(req_or_item, spider)
                except Exception as exc:
                    log.critical(u'process_item() raised %r:\n%s'
                                 % (exc, traceback.format_exc()),
                                 extra=dict(spider=spider))
                else:
                    yield item
            elif isinstance(req_or_item, Request):
                try:
                    request = self.process_request(response, req_or_item, spider)
                except IgnoreRequest:
                    self.stats.inc_value('requests_for_dup_items_skipped')
                else:
                    yield request
            else:
                log.error('wtf? why did the spider yield this?')

    def process_request(self, resp, req, spider):
        source_id = getattr(spider, 'source_id', None)
        item_id_in_source = req.meta.get('item_id_in_source', None)
        if source_id is None or item_id_in_source is None:
            return req
        item_id = self.dup_item(source_id, item_id_in_source)
        if item_id is not None:
            log.debug(u'Dublicate item for %(url)r: item_id=%(item_id)d',
                      dict(url=req.url, item_id=item_id),
                      extra=dict(spider=spider, request=req, response=resp))
            raise IgnoreRequest
        return req

    def dup_item(self, source_id, item_id_in_source):
        index_key = (source_id, item_id_in_source)
        if index_key in self.dupfilter_cache:
            log.debug(
                u'already cached %r→%r' % (index_key,
                                           self.dupfilter_cache[index_key])
            )
            return self.dupfilter_cache[index_key]
        with self.conn.cursor() as cur:
            cur.execute(' SELECT item_id FROM bot_items.items'
                        ' WHERE (source_id, item_id_in_source) = %s;',
                        (index_key,))
            (item_id,), = cur.fetchall() or [[None]]
            if item_id is None:
                return None
            return self.dupfilter_cache.setdefault(index_key, item_id)

    def process_item(self, item, spider):
        # Set default values
        item.setdefault('body', u'')
        item.setdefault('author', None)
        item.setdefault('author_id', None)
        item.setdefault('url', None)
        if not isinstance(item, FacebookPost):
            if item['pubdate'].tzinfo is None:  # In absence of TZ set to BG
                item['pubdate'] = BG_TZ.localize(item['pubdate'])
            else:
                item['pubdate'] = item['pubdate'].astimezone(BG_TZ)

        try:
            self.validate_item(item)
        except Exception as exc:
            log.critical(
                u'validate_item() raised {!r}:\n{}'.format(
                    exc, traceback.format_exc()),
                extra=dict(spider=spider),
            )

        with self.conn.cursor() as cur:
            self._conditional_insert(cur, item)
        return item

    def _conditional_insert(self, cur, item):

        if isinstance(item, Tweet):
            self.store_tweet(cur, item)
            return
        if isinstance(item, FacebookPost):
            self.store_facebookpost(cur, item)
            return
        if not isinstance(item, Article):
            log.error(u'Skipping item because not an article: %s' % item)
            return

        log.debug('Processing item %s' % item)

        # TODO: Add date check in the search query. #WONTFIX
        if item['id']:
            item_id = self.dup_item(item['source_id'], item['id'])
        else:
            # warn in this case
            cur.execute("SELECT item_id FROM bot_items.items WHERE source_id = %s AND url = %s ",
                        (item['source_id'], item['url']))
            item_id, = cur.fetchone() or [None]
        if item_id is not None:
            log.debug(u"Item already stored in db: %s" % item)
            self.stats.inc_value('item_already_stored')
            return

        cur.execute(
            "INSERT INTO bot_items.items (source_id, title, body, author, author_id, url, item_id_in_source, original_date)"
            " VALUES ( %s, %s, %s, %s, %s, %s, %s, %s )",
            (item['source_id'],
             item['title'], item['body'], item['author'],
             item['author_id'],
             item['url'], item['id'], item['pubdate'])
        )
        log.debug(u"Item stored in db: %s" % item)

    def _render_xml(self, xml):
        try:
            if not xml.strip() or xml.startswith('<!--'):
                return u''
                # Due to BUG https://bugs.launchpad.net/lxml/+bug/761215
                # if an error about parsing an empty document occurs
                # the previous error will be shown instead!
            rendered = lxml.html.fromstring(xml).text_content().strip()
            return rendered
        except Exception as exc:
            log.critical(
                u'validate_item() raised {!r}:\n{}'.format(
                    exc, traceback.format_exc()))
        return xml

    def validate_item(self, item):

        class limits(object):
            title_lower = 3
            title_upper = 1000
            body_lower = 5
            body_upper = 1000000,
            title_body_lower = 50

        class lengths(object):
            title = len(item.get('title') or u'')
            body = len(self._render_xml(item.get('body') or u''))

        url = item['url']
        if url is None:
            log.critical(u'Item lacks url: {}'.format(item))

        context = lengths, limits, url
        if not isinstance(item, (Tweet, FacebookPost)):
            if lengths.title < limits.title_lower:
                log.warning(u"Title validation failed (length {0.title} < {1.title_lower}) in url: {2!r}".format(*context))
            elif lengths.title > limits.title_upper:
                log.warning(u"Title validation failed (length {0.title} > {1.title_upper}) in url: {2!r}".format(*context))
        if lengths.body < limits.body_lower:
            log.warning(u"Body validation failed (length {0.body} < {1.body_lower}) in url: {2!r}".format(*context))
        elif lengths.body > limits.body_upper:
            log.warning(u"Body validation failed (length {0.body} > {1.body_upper}) in url: {2!r}".format(*context))
        if not isinstance(item, (Tweet, FacebookPost)):
            if lengths.title + lengths.body < limits.title_body_lower:
                log.warning(u"Title and body validation failed (length {0.title} + {0.body} < {1.title_body_lower}) in url: {2!r}".format(*context))

        if not isinstance(item, FacebookPost):
            # TODO: also check facebook
            now = dt.datetime.now(BG_TZ)
            if item['pubdate'] > now:
                log.warning(
                    u"PubDate validation failed ({} > {} (now)) in url: {}".format(
                        item['pubdate'], now, url))
        # TODO: Existing modules could check this
        url_regex = re.compile(
            r'^(?:http|ftp)s?://'
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?))'
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if re.search(url_regex, item['url']) is None:
            log.warning(u"URL validation failed: %r" % item['url'])

    def store_tweet(self, cur, item):
        log.debug(u'Processing tweet %s' % item)
        cur.execute(
            "SELECT items.twitter_add_update_item(%s, %s, %s, %s::timestamp, %s, %s, %s, %s, %s, %s, %s, %s)",
            (item['body'], item['author'],
             item['author_id'],
             item['pubdate'], item['url'], item['id_in_source'],
             item['shares'],
             item['source_name'], item['source_description'],
             item['source_url'], item['source_id_in_source'],
             item['source_country_id'])
        )
        log.debug(u"Tweet stored in db: %s" % item)

    def store_facebookpost(self, cur, item):
        log.debug(u'Processing facebook post %s' % item)
        post = (
            None,                       # item_id
            None,                       # first_item_id
            None,                       # previous_item_id
            None,                       # parent_item_id
            item['source_id'],          # source_id
            None,                       # title
            item['body'],               # body
            item['author'],             # author
            item['author_id'],          # author_id
            item['original_date'],      # original_date
            None,                       # last_updated
            item['url'],                # url
            item['item_id_in_source'],  # item_id_in_source
            item.get('shares'),         # shares
            item['positive_votes'],    # positive_votes
            None,                       # negative_votes
            False,                      # removed
            False,                      # publishing_processed
            0,                          # publishing_retries
            None,                       # meta_profile_id
            None,                       # created_on
        )

        comments = []
        for comment in item['comments']:
            comments += [(
                None,                          # item_id
                None,                          # first_item_id
                None,                          # previous_item_id
                None,                          # parent_item_id
                comment['source_id'],          # source_id
                None,                          # title
                comment['body'],               # body
                comment['author'],             # author
                comment['author_id'],          # author_id
                comment['original_date'],      # original_date
                None,                          # last_updated
                comment['url'],                # url
                comment['item_id_in_source'],  # item_id_in_source
                None,                          # shares
                comment['positive_votes'],    # positive_votes
                None,                          # negative_votes
                False,                         # removed
                False,                         # publishing_processed
                0,                             # publishing_retries
                None,                          # meta_profile_id
                None,                          # created_on
            )]
        cur.execute("SELECT items.facebook_add_update_item(%s::bot_items.items, %s::bot_items.items[] )", (post, comments))
        log.debug(u"Facebook post stored in db: %s" % item)
