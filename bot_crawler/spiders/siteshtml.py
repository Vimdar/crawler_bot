# -*- coding: utf-8
import pprint as pp
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Spider
from scrapy.http import Request
from bot_crawler.items import *
from urlparse import urlparse
import re, pytz  # , dateutil.parser
import datetime
from bot_crawler.selector import Selector, normalize_whitespace
from bot_crawler import profiling


# TODO: replace with newspaper(python 3 only)
# import imp
# imp.find_module('boilerpipe')  # Assert it exists without importing it
# def boilerpipe_extract(extractor, body):
#     from boilerpipe.extract import Extractor
#     body = Extractor(extractor=extractor, html=body)
#     return u'<pre>' + xml_escape(body.getText()) + u'</pre>'

class SitesHTMLSpider(Spider):
    name = 'siteshtml'

    def __init__(self, **kwargs):
        kwargs = {k.decode('utf-8'): v.decode('utf-8') for k, v in kwargs.items()}
        self.logger.debug(u'Spider arguments:\n{}'.format(pp.pformat(kwargs)))
        Spider.__init__(self, **kwargs)

        self.start_urls = kwargs['start_urls'].split(';')

        if 'allowed_domains' in kwargs and kwargs['allowed_domains'] is not None:
            self.allowed_domains = kwargs['allowed_domains'].split(';')
        else:
            self.allowed_domains = []
            for url in self.start_urls:
                parsed_url = urlparse(url)
                self.allowed_domains.append(parsed_url.hostname)

        self.source_id = kwargs['source_id']
        self.articles_xpath = kwargs['articles_xpath']
        self.title_xpath = kwargs['title_xpath']

        self.body_xpath = kwargs.get('body_xpath')
        self.extractor = kwargs.get('extractor')
        if self.body_xpath is None and self.extractor is None:
            raise TypeError('__init__ needs `body_xpath` and/or `extractor`')
        self.menu_xpath = kwargs.get('menu_xpath')
        self.sections_pagination_xpath = kwargs.get('sections_pagination_xpath')
        self.author_xpath = kwargs.get('author_xpath')
        self.pagination_depth = int(kwargs.get('pagination_depth', '0'))

        self.link_id_regex = kwargs.get('link_id_regex')  # id from link
        self.url_id_regex = kwargs.get('url_id_regex')  # gen id from final url

    def parse(self, response):
        if self.menu_xpath is not None:
            lext = LinkExtractor(restrict_xpaths=(self.menu_xpath))
            for link in lext.extract_links(response):
                yield Request(link.url, callback=self.parse)
        if self.sections_pagination_xpath is not None:
            pg_cnt = response.meta.get('pg_cnt', self.pagination_depth)
            if 0 <= pg_cnt:
                self.logger.debug('Max pagination depth hit on %r' % response)
            else:
                lext = LinkExtractor(restrict_xpaths=self.sections_pagination_xpath)
                for link in lext.extract_links(response):
                    yield Request(link.url, meta={'pg_cnt': pg_cnt - 1})
        pubdate = datetime.datetime.now()
        lext = LinkExtractor(restrict_xpaths=(self.articles_xpath))
        for link in lext.extract_links(response):
            meta = {'link': link.url, 'pubdate': pubdate}
            if self.url_id_regex is not None and self.link_id_regex is None:
                meta['item_id_in_source'] = link.url
            if self.link_id_regex is not None:
                meta['item_id_in_source'] = get_regex_match(
                    link.url, self.link_id_regex, 'id') or link.url
            yield Request(link.url, callback=self.parse_article, meta=meta)

    def parse_article(self, response):
        item = Article(
            source_id=self.source_id,
            url=response.url,
            pubdate=response.meta['pubdate'],
            author=None,
        )
        with profiling.timeit(self, 'xml_parsing_time'):
            hxs = Selector(response, type='html')
        item['title'] = normalize_whitespace(u' '.join(hxs.xpath(self.title_xpath).render()))
        if self.author_xpath is not None:
            item['author'] = normalize_whitespace(u' '.join(hxs.xpath(self.author_xpath).render()))
        item['body'] = self.extract_body(hxs, response)

        item['id'] = response.meta.get('item_id_in_source', response.url)
        if self.url_id_regex:
            matched_id = get_regex_match(response.url, self.url_id_regex, 'id')
            item['id'] = matched_id or item['id']
        return item

    def extract_body(self, hxs, response):
        body = response.body_as_unicode()
        if self.body_xpath is not None:
            body = u"\n".join(hxs.xpath(self.body_xpath).sane_xml())
        # if self.extractor is not None:
        #     body = boilerpipe_extract(self.extractor, body)
        return body


def get_regex_match(string, regex, regex_group_name):
    match = re.search(regex, string)
    if match and (regex_group_name in match.groupdict()):
        return match.group(regex_group_name)
