# -*- coding: utf-8
import scrapy
import pprint as pp
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Spider
from scrapy.http import Request
from urlparse import urlparse
from scrapy import Selector
# from js_bot.selector import Selector, normalize_whitespace
from bot_crawler.items import *
import re
from scrapy_splash import SplashRequest
import datetime
# from scrapy.utils.log import configure_logging

# TODO: replace with newspaper(python 3 only)
# import imp
# imp.find_module('boilerpipe')  # Assert it exists without importing it
# def boilerpipe_extract(extractor, body):
#     from boilerpipe.extract import Extractor
#     body = Extractor(extractor=extractor, html=body)
#     return u'<pre>' + xml_escape(body.getText()) + u'</pre>'


class VSpider(scrapy.Spider):
    name = 'vspider'

    def __init__(self, **kwargs):
        kwargs = {k.decode('utf-8'): v.decode('utf-8') for k, v in kwargs.items()}
        self.logger.debug(u'Spider arguments:\n{}'.format(pp.pformat(kwargs)))
        Spider.__init__(self, **kwargs)

        self.start_urls = kwargs['start_urls'].split(';')
        self.mode = kwargs.get('mode') or 'render'
        self.init_wait = kwargs.get('init_wait') or 1
        self.pagination_wait = kwargs.get('pagination_wait') or 1
        self.article_wait = kwargs.get('article_wait') or 1

        if 'allowed_domains' in kwargs and kwargs['allowed_domains'] is not None:
            self.allowed_domains = kwargs['allowed_domains'].split(';')
        else:
            self.allowed_domains = []
            for url in self.start_urls:
                parsed_url = urlparse(url)
                self.allowed_domains.append(parsed_url.hostname)

        self.source_id = kwargs.get('source_id')
        self.articles_xpath = kwargs.get('articles_xpath')
        self.title_xpath = kwargs['title_xpath']
        self.menu_xpath = kwargs.get('menu_xpath')
        self.sections_pagination_xpath = kwargs.get('sections_pagination_xpath')
        self.pagination_depth = kwargs.get('pagination_depth')

        self.body_xpath = kwargs.get('body_xpath')
        self.extractor = kwargs.get('extractor')
        if self.body_xpath is None and self.extractor is None:
            raise TypeError('__init__ needs `body_xpath` and/or `extractor`')

        self.author_xpath = kwargs.get('author_xpath')
        self.pub_date = datetime.datetime.now()
        self.link_id_regex = kwargs.get('link_id_regex')  # id from link
        self.url_id_regex = kwargs.get('url_id_regex')  # gen id from final url
        self.redirects = False
        self.loadmore = kwargs.get('loadmore') or 'button'
        self.loadmore_depth = kwargs.get('loadmore_depth') or 1
        # if (self.mode == 'button' and self.loadmore is None) or (self.mode == 'button' and self.loadmore_depth is None):
        #     self.mode = 'render'
        self.script_button = """
            function main(splash)
            local url = splash.args.url
            assert(splash:go(url))
            assert(splash:wait(3))
            local submit_button = splash:select('%s')
            for i = 1,%s do
              submit_button:click()
              assert(splash:wait(2))
            end
            return splash:html()
            end""" % (self.loadmore, self.loadmore_depth)
        self.scroll_num = kwargs.get('scrolls') or 1

        self.script_scroll = """
            function main(splash)
            assert(splash:autoload("https://code.jquery.com/jquery-3.1.1.min.js"))
            local num_scrolls = %s
            local scroll_delay = 1
            local scroll_to = splash:jsfunc("window.scrollTo")
            local get_body_height = splash:jsfunc(
                "function() {return document.body.scrollHeight;}"
            )
            assert(splash:go(splash.args.url))
            splash:wait(splash.args.wait)

            for i = 1, num_scrolls do
                scroll_to(0, get_body_height())
            splash:wait(scroll_delay)
            end
            return splash:html()
            end""" % (self.scroll_num)

    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(
                url, self.parse, endpoint='render.html',
                args={'wait': self.init_wait}
            )

    def parse(self, response):
        if self.menu_xpath is not None:
            lext = LinkExtractor(restrict_xpaths=(self.menu_xpath))
            for link in lext.extract_links(response):
                yield self.generate_splash_request(link)

        if self.sections_pagination_xpath is not None:
            pg_cnt = response.meta.get('pg_cnt', self.pagination_depth)
            if pg_cnt >= 0:
                self.logger.debug('Max pagination depth hit on %r' % response)
            else:
                lext = LinkExtractor(restrict_xpaths=self.sections_pagination_xpath)
                for link in lext.extract_links(response):
                    yield self.generate_splash_request(link, pg_cnt)

        pubdate = datetime.datetime.now()
        lext = LinkExtractor(restrict_xpaths=(self.articles_xpath))
        for link in lext.extract_links(response):
            self.logger.info('-Sending req to: ------------' + link.url)
            meta = {'link': link.url, 'pubdate': pubdate}
            if self.link_id_regex is not None:
                meta['item_id_in_source'] = get_regex_match(
                    link.url, self.link_id_regex, 'id') or link.url
            yield SplashRequest(link.url, callback=self.parse_article, meta=meta, args={'wait': self.article_wait})

    def generate_splash_request(self, link, pg_cnt=None):
        meta = {}
        if pg_cnt is not None:
            meta = {'pg_cnt': pg_cnt - 1}
            # args = {wait: init_wait}
        if self.mode == 'button':
            return SplashRequest(
                link.url, self.parse, endpoint='execute',
                args={'lua_source': self.script_button, 'wait': self.pagination_wait}, meta=meta
            )
        elif self.mode == 'scroll':
            return SplashRequest(
                link.url, self.parse, endpoint='execute',
                args={'lua_source': self.script_scroll, 'wait': self.pagination_wait}, meta=meta
            )

        return SplashRequest(
            link.url, self.parse, endpoint='render.html',
            args={'wait': 1}, meta=meta
        )

    def parse_article(self, response):
        self.logger.info('-Make article out of:---------' + response.url)
        article = Article(
            source_id=self.source_id,
            url=response.url,
            pubdate=self.pub_date,
            author=None,
        )

        hxs = Selector(response, type='html')
        article['title'] = re.sub('<[^<]+?>', '', ''.join(hxs.xpath(self.title_xpath).extract()))
        article['body'] = self.extract_body(hxs, response)
        if self.author_xpath is not None:
            # item['author'] = normalize_whitespace(u' '.join(hxs.xpath(self.author_xpath).render()))
            article['author'] = u' '.join(hxs.xpath(self.author_xpath).render())

        article['id'] = response.meta.get('id', response.url)
        if self.url_id_regex:
            matched_id = get_regex_match(response.url, self.url_id_regex, 'id')
            article['id'] = matched_id or article['id']

        yield article

    def extract_body(self, hxs, response):
        body = response.body_as_unicode()
        if self.body_xpath is not None:
            body = re.sub('<[^<]+?>', '', u"\n".join(hxs.xpath(self.body_xpath).extract()))
        # if self.extractor is not None:
        #     body = boilerpipe_extract(self.extractor, body)
        return body


def get_regex_match(string, regex, regex_group_name):
    match = re.search(regex, string)
    if match and (regex_group_name in match.groupdict()):
        return match.group(regex_group_name)
