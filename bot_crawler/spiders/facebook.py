# -*- coding: utf-8
import re, json
import traceback
from xml.sax.saxutils import escape as xml_escape
import datetime as dt
import calendar
import urlparse

import pytz
import psycopg2, psycopg2.extras
from scrapy.spiders import Spider
from scrapy.http import Request

from bot_crawler.items import FacebookPost
from scrapy.utils.project import get_project_settings

BG_TZ = pytz.timezone('Europe/Sofia')


def wrap_fb_link(item):
    if 'link' not in item:
        return u''
    header_format = (
        u'<div>'
        u'<h3><a href="{link}" title="{caption}">{name}</a> - {caption}</h3>'
        u'<i>{description}</i>'
        u'</div><br />')
    fields = {
        k: xml_escape(item.get(k, u''))
        for k in ['name', 'caption', 'description']}
    fields['link'] = item['link'].replace('"', '%22')
    return header_format.format(**fields)


def wrap_paragraphs(text):
    return u'\n'.join(
        u'<p style="max-width: 470px;">{}</p>'.format(line)
        for line in xml_escape(text).split('\n')
        if line
    )


def wrap_fb_text(item):
    return u'\n'.join(
        u'<div class="{}" style="max-width: 470px;">{}</div>'.format(
            field, wrap_paragraphs(item[field]))
        for field in ['message', 'story']
        if item.get(field, u'').strip() != u''
    )


def parse_fburl(url):
    'Parse a facebook url and guess the part that is can be the text id and its type'
    scheme, netloc, path, query, fragment = urlparse.urlsplit(url)
    assert netloc.rsplit('.', 2)[-2:] == ['facebook', 'com'], 'Not a fb url'
    path = path.rstrip('/').split('/')
    assert path.pop(0) == '' and path, 'Incomplete url'

    if 'groups' == path[0]:
        # /groups/378507035627441/
        # /groups/plevneliev
        return ('group_id' if path[1].isdigit() else 'group_alias', path[1])
    # /pages/Земеделски-народен-съюз-ЗНС/233970676790649
    if 'pages' == path[0]:
        return ('page_id', path[2])
    # /Politikacom-1794879394072692
    if 1 == len(path) and path[0].rsplit('-', 2)[-1].isdigit():
        return ('page_alias_with_id', path[0].rsplit('-', 2)[-1])
    # /profile.php?id=100008011023153
    if 'profile.php' == path[0]:
        query = urlparse.parse_qs(query)
        id_, = query['id']
        return ('profile_id', id_)
    # /dps.ahmed.dogan
    return (None, path[0])


def get_pubdate(pubdate_str):
    pubdate = dt.datetime.strptime(pubdate_str, "%Y-%m-%dT%H:%M:%S+0000")
    pubdate = pytz.timezone('UTC').localize(pubdate)
    return pubdate.astimezone(BG_TZ)


class FacebookSpider(Spider):
    name = 'facebook'

    handle_httpstatus_list = [400]
    facebook_graph_url = 'https://graph.facebook.com/v2.8/'
    revisit_limit = dt.timedelta(hours=8)  # how old news to revisit
    items_limit = '100'

    @classmethod
    def from_crawler(cls, crawler, **kwargs):
        return cls(crawler, **kwargs)

    def __init__(self, crawler, **kwargs):
        kwargs = {k.decode('utf-8'): v.decode('utf-8') for k, v in kwargs.items()}
        self.crawler = crawler
        self.settings = get_project_settings()

        self.conn = psycopg2.connect(
            host=self.settings.get('CRAWLERDB_STORE_DBHOST'),
            port=self.settings.get('CRAWLERDB_STORE_DBPORT'),
            database=self.settings.get('CRAWLERDB_STORE_DATABASE'),
            user=self.settings.get('CRAWLERDB_STORE_DBUSER'),
            password=self.settings.get('CRAWLERDB_STORE_DBPASSWORD'),
            cursor_factory=psycopg2.extras.RealDictCursor,
        )
        self.conn.autocommit = True
        with self.conn.cursor() as cur:
            cur.execute("SET application_name = 'media_crawler:facebook_spider';")

    def start_requests(self):
        with self.conn.cursor() as cur:
            # get access_token values for fb api from credentials table(pool)
            cur.execute('SELECT access_token FROM items.facebook_credentials;')
            access_tokens = [at for at, in [atl.values() for atl in cur]]

            cur.execute('''
                SELECT s.source_id, s.source_id_in_source, s.url,
                       COALESCE(s.last_check, now()::timestamp without time zone) AS last_check
                FROM items.source s JOIN items.source_type t USING(source_type_id)
                WHERE t.text_id = 'facebook'
                  AND active IS TRUE
                ORDER BY s.source_id_in_source IS NOT NULL, s.last_check NULLS FIRST;''')
            sources_to_fetch = cur.fetchall()

        for source in sources_to_fetch:
            access_token = access_tokens[source['source_id'] % len(access_tokens)]
            since = source['last_check'] - self.revisit_limit
            since = since.replace(tzinfo=BG_TZ)

            if not source['source_id_in_source']:
                self.logger.warning(
                    'source_id %d (%r) lacks page id, will get it from the api'
                    % (source['source_id'], source['url'])
                )
                try:
                    req = self.mk_fbid_req(source['url'], source['source_id'],
                                           access_token=access_token)
                except:
                    self.logger.exception(
                        'Cannot guess facebook id from url %r' % source['url']
                    )
                else:
                    yield req
                continue

            yield Request(
                '{0}{1}/feed?limit={2}&access_token={3}&since={4}&'
                'fields=message,created_time,id,story,from,likes.limit(0).summary(true),'
                'shares,comments.limit(0).summary(true)'.format(
                    self.facebook_graph_url, source['source_id_in_source'], self.items_limit,
                    access_token, calendar.timegm(since.utctimetuple())),
                callback=self.parse_posts,
                meta={'source_id': source['source_id'], 'page': 0,
                      'source_id_in_source': source['source_id_in_source'],
                      'access_token': access_token,
                      'since': since}
            )

    def mk_fbid_req(self, url, source_id, access_token):
        'Create a request to find the facebook id'
        scheme, netloc, path, query, fragment = urlparse.urlsplit(url)
        assert netloc.rsplit('.', 2)[-2:] == ['facebook', 'com'], 'Not a fb url'
        path = path.split('/')
        assert path.pop(0) == '' and path, 'Incomplete url'

        type_, id_ = parse_fburl(url)
        if type_ in (None, 'page_id', 'page_alias_with_id', 'group_id', 'profile_id'):
            return Request(
                '%s%s?access_token=%s' % (self.facebook_graph_url, id_, access_token),
                callback=self.set_fbid,
                meta={'source_id': source_id,
                      'source_id_in_source': None,
                      'fbstrid': id_},
            )
        elif type_ == 'group_alias':
            assert set('&?=#').isdisjoint(path[1]), 'Illegal url: %r' % path[1]
            return Request(
                '{0}search?type=group&q={1}&fields=id,email&limit=32&access_token={2}'.format(
                    self.facebook_graph_url, path[1], access_token
                ),
                callback=self.set_fbid_group_search,
                meta={'source_id': source_id, 'fbstrid': id_,
                      'source_id_in_source': None,
                      'access_token': access_token},
            )
        raise NotImplementedError('Unknown alias/id type %r' % type_)

    def set_fbid_group_search(self, response):
        search = json.loads(response.body_as_unicode())
        fbstrid = response.meta['fbstrid']
        self.logger.info('Searching for %r in %r' % (fbstrid, search))
        group, = [g for g in search['data']
                  if g['email'].partition('@')[0] == fbstrid] or [None]
        if group is None:
            self.logger.error(u'Failed to find group %r in search results: %r',
                              (fbstrid, search))
            return
        assert group['email'].partition('@')[2] == 'groups.facebook.com', \
            'Unexpected group email %r' % group['email']
        assert group['id'].isdigit(), 'Search for group %r yielded non-numeric id: %r' % (fbstrid, search)
        yield Request(
            '%s%s?access_token=%s' % (self.facebook_graph_url, group['id'],
                                      response.meta['access_token']),
            callback=self.set_fbid,
            meta={'source_id': response.meta['source_id'],
                  'source_id_in_source': group['id'],
                  'fbstrid': fbstrid},
        )

    def set_fbid(self, response):
        if 200 != response.status:
            try:
                error = json.loads(response.body_as_unicode())['error']
            except:
                pass
            else:
                if self.handle_error(response, error):
                    return
            self.logger.error(
                'Unhandled API error (source_id=%s), %s responded with:\n%s'
                % (response.meta['source_id'], response.url,
                   response.body_as_unicode()))
            return
        info = json.loads(response.body_as_unicode())
        fbid, name = info['id'], info['name']
        self.logger.info('Saving facebook id %s of source_id %s (%r)'
                         % (fbid, response.meta['source_id'],
                            response.meta['fbstrid']))
        with self.conn.cursor() as cur:
            cur.execute('UPDATE items.source SET source_id_in_source = %s'
                        ' WHERE source_id = %s;',
                        (fbid, response.meta['source_id']))

    def handle_error(self, response, error):
        if 21 == error.get('code'):
            old, new = re.search(r'^Page ID (\d+) was migrated to page ID (\d+)\.'
                                 '  Please update your API calls to the new ID$',
                                 error['message']).groups()
            old, new = int(old), int(new)
            self.logger.warning(
                'source_id %d: Replacing source_id_in_source %r with %r because %s responded with:\n%s'
                % (response.meta['source_id'], response.meta['source_id_in_source'], new, response.url, response.body_as_unicode()))
            with self.conn.cursor() as cur:
                cur.execute('UPDATE items.source SET source_id_in_source = %s'
                            ' WHERE source_id = %s;',
                            (new, response.meta['source_id']))
                return True

    def parse_posts(self, response):
        if 200 != response.status:
            try:
                error = json.loads(response.body_as_unicode())['error']
            except:
                pass
            else:
                if self.handle_error(response, error):
                    return
            self.logger.error(
                'Unhandled API error (source_id=%s), %s responded with:\n%s'
                % (response.meta['source_id'], response.url,
                   response.body_as_unicode()))
            return

        with self.conn.cursor() as cur:
            cur.execute(
                'UPDATE items.source SET last_check = %s WHERE source_id = %s;',
                (dt.datetime.now(tz=BG_TZ),
                 response.meta['source_id']))

        logfmt = '[source {source_id_in_source} / page {page}]'.format(**response.meta)
        source_id_in_source = response.meta['source_id_in_source']

        items = json.loads(response.body)
        self.logger.debug('%s has %d posts' % (logfmt, len(items['data'])))
        for item in items['data']:
            post = FacebookPost(source_id=response.meta['source_id'])
            post['body'] = wrap_fb_link(item) + u'\n' + wrap_fb_text(item)
            post['author'] = xml_escape(item['from']['name'])
            post['author_id'] = item['from']['id']
            post['original_date'] = get_pubdate(item['created_time'])
            if '_' in item['id']:
                self.crawler.stats.inc_value('facebook/composite_post_ids_count')
                page_id, post_id = item['id'].split('_')
                assert page_id == source_id_in_source,\
                    'page_id %r != source_id_in_source %r'\
                    % (page_id, source_id_in_source)
            else:
                self.crawler.stats.inc_value('facebook/independent_post_ids_count')
                page_id, post_id = source_id_in_source, item['id']
                assert post_id.isdigit(), 'post_id.isdigit(): %r' % post_id
                assert len(post_id) > 14, 'len(post_id) > 14: %r' % post_id
            post['item_id_in_source'] = '%s_%s' % (page_id, post_id)

            post['url'] = 'https://www.facebook.com/{}/posts/{}'.format(
                page_id, post_id)
            post['shares'] = item.get('shares', {}).get('count')
            try:
                post['positive_votes'] = item['likes']['summary']['total_count']
            except KeyError as exc:
                self.logger.warning(u'No "likes" in post {} at {}:\n{}'.format(
                        item['id'], response.url, traceback.format_exc()))

            post['comments'] = []
            comment_count = item['comments']['summary']['total_count']
            if not comment_count:
                yield post
            self.logger.debug(
                '%s: %s has %d comments' % (logfmt, item['id'], comment_count))
            yield Request(
                '{0}{1}/comments?filter=stream&limit={2}&access_token={3}&fields=message,'
                'created_time,id,from,likes.limit(0).summary(true)'.format(
                    self.facebook_graph_url, post['item_id_in_source'],
                    self.items_limit, response.meta['access_token']),
                callback=self.parse_comments,
                meta={'post': post, 'page': 0,
                      'source_id': response.meta['source_id'],
                      'source_id_in_source': response.meta['source_id_in_source'],
                      'item_id_in_source': post['item_id_in_source'],
                      'access_token': response.meta['access_token']},
            )

            if response.meta['since'] > post['original_date']:
                self.logger.debug('%s: pagination end' % logfmt)
                return

        if 'next' in items.get('paging', {}):
            yield Request(items['paging']['next'], callback=self.parse_posts,
                          meta={'source_id': response.meta['source_id'],
                                'page': 1 + response.meta['page'],
                                'source_id_in_source': response.meta['source_id_in_source'],
                                'item_id_in_source': post['item_id_in_source'],
                                'since': response.meta['since'],
                                'access_token': response.meta['access_token']})

    def parse_comments(self, response):
        if 200 != response.status:
            self.logger.error(
                '%s responded with:\n%s' % (response.url, response.body_as_unicode()))
            return
        post = response.meta['post']
        logfmt = '[source {source_id_in_source} / post {post[item_id_in_source]} / page {page}]'.format(**response.meta)
        source_id_in_source = response.meta['source_id_in_source']
        item_id_in_source = response.meta['item_id_in_source']
        page_id, post_id = item_id_in_source.split('_')

        items = json.loads(response.body)

        self.logger.debug('%s has %d comments' % (logfmt, len(items['data'])))
        for item in items['data']:
            comment = FacebookPost(source_id=post['source_id'])
            comment['body'] = u'<pre>%s</pre>' % xml_escape(item['message'])

            try:
                author_name = item['from']['name']
            except KeyError:
                author_name = None
            comment['author'] = xml_escape(author_name) if author_name else None

            try:
                author_id = item['from']['name']
                comment['author_id'] = author_id
            except KeyError:
                comment['author_id'] = None

            comment['original_date'] = get_pubdate(item['created_time'])
            if '_' in item['id']:
                self.crawler.stats.inc_value('facebook/composite_comment_ids_count')
                _post_id, comment_id = item['id'].split('_')
                assert post_id == _post_id,\
                    'post_id %r != 1st part of composite id %r'\
                    % (post_id, _post_id)
            else:
                self.crawler.stats.inc_value('facebook/independent_comment_ids_count')
                _post_id, comment_id = post_id, item['id']
                assert comment_id.isdigit(), 'comment_id.isdigit(): %r' % comment_id
                assert len(comment_id) > 14, 'len(comment_id) > 14: %r' % comment_id
            comment['item_id_in_source'] = '%s_%s' % (_post_id, comment_id)

            source_id_in_source, page_id = post['item_id_in_source'].split('_')
            comment['url'] = 'https://www.facebook.com/{}/posts/{}?comment_id={}'.format(
                source_id_in_source, page_id, comment_id)
            comment['positive_votes'] = item['likes']['summary']['total_count']
            post['comments'].append(comment)

        if 'next' in items.get('paging', {}):
            yield Request(
                items['paging']['next'], callback=self.parse_comments,
                meta={'post': post, 'page': 1 + response.meta['page'],
                      'source_id': response.meta['source_id'],
                      'source_id_in_source': response.meta['source_id_in_source'],
                      'item_id_in_source': post['item_id_in_source']})
        else:
            self.logger.debug('%s: pagination end' % logfmt)
            yield post
