# -*- coding: utf-8
from collections import defaultdict
# import dateutil.parser
# TODO use the parser again
import datetime as dt
import re, pytz
from itertools import izip_longest
from urlparse import urlparse
import pprint as pp

from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Spider
from scrapy.http import Request

from bot_crawler.items import Article as Item
from bot_crawler.selector import Selector, SelectorList, normalize_whitespace
from bot_crawler import profiling


BG_TZ = pytz.timezone('Europe/Sofia')


def regex_groupd(string, regex):
    match = re.search(regex, string)
    return defaultdict(lambda: None, match.groupdict() if match else {})


class ForumsHTMLSpider(Spider):
    name = 'forumshtml'

    def __init__(self, **kwargs):
        kwargs = {k.decode('utf-8'): v.decode('utf-8') for k, v in kwargs.items()}
        self.logger.debug(u'Spider arguments:\n{}'.format(pp.pformat(kwargs)))

        self.start_urls = kwargs['start_urls'].split(';')
        self.allowed_domains = [urlparse(url).hostname for url in self.start_urls]
        if kwargs.get('allowed_domains') is not None:
            self.allowed_domains += kwargs['allowed_domains'].split(';')

        req_args = ('source_id', 'id_xpath', 'author_xpath', 'body_xpath',)
        vars(self).update({arg: kwargs[arg] for arg in req_args})

        opt_args = ('forums_xpath', 'topics_xpath', 'topic_pagination_xpath',
                    'topic_pagination_limit', 'pubdate_xpath', 'id_regex',
                    'post_link_xpath', 'post_container_xpath',
                    'post_group_xpath', 'title_xpath',)
        vars(self).update({arg: kwargs.get(arg) for arg in opt_args})

        if self.topic_pagination_limit is None:
            self.topic_pagination_limit = 1
        self.topic_pagination_limit = int(self.topic_pagination_limit)

    def parse(self, response):
        'Yield requests to subforums and topics with the right callbacks'
        # TODO: New kwarg for this, move to new method
        if re.match(r'(application|text)/((rss|rdf|atom)\+)?xml', response.headers['Content-Type']):
            self.logger.debug('%s is a feed, parsing as such' % response.url)
            with profiling.timeit(self, 'xml_parsing_time'):
                xxs = Selector(response, type='xml')
            # URLs are already % escaped, don't re-encode then
            links = xxs.xpath("//channel//item//link/text()|//feed//entry//link/@href").extract()
            for link in links:
                yield Request(link and response.urljoin(link), callback=self.parse_topic)
            return

        if self.forums_xpath is not None:
            lext = LinkExtractor(restrict_xpaths=(self.forums_xpath))
            for link in lext.extract_links(response):
                yield Request(link.url, callback=self.parse)

        if self.topics_xpath is None:
            for item_or_req in self.parse_topic(response):
                yield item_or_req
        else:
            lext = LinkExtractor(restrict_xpaths=(self.topics_xpath))
            for link in lext.extract_links(response):
                yield Request(link.url, callback=self.parse_topic,
                              meta={'topic': link.text})

    def parse_topic(self, response):
        'Yield post items'
        with profiling.timeit(self, 'xml_parsing_time'):
            hxs = Selector(response, type='html')

        tpage_depth = response.meta.get('topic_pagination_depth', 0)
        if self.topic_pagination_xpath is not None and tpage_depth <= self.topic_pagination_limit:
            lext = LinkExtractor(
                restrict_xpaths=(self.topic_pagination_xpath))
            for link in (lext.extract_links(response)):
                request = Request(link.url, callback=self.parse_topic)
                if 'topic' in response.meta:
                    request.meta['topic'] = response.meta['topic']
                request.meta['topic_pagination_depth'] = tpage_depth + 1
                self.logger.debug(u'Traversing pagination %s' % link)
                yield request

        for post in self.parse_posts(response, hxs):
            yield post

    def parse_posts(self, response, hxs):
        parse = self.parse_posts_grouped
        if self.post_container_xpath is None:
            parse = self.parse_posts_aligned
        for post_idx, post in parse(response, hxs):
            try:
                post['pubdate'] = get_pubdate(post['pubdate'])
            except:
                self.logger.exception(u'Cannot parse date %d at %s %r'
                                      % (post_idx, response.url, post.pop('pubdate')))
            post['url'] = post.get('url') and response.urljoin(post['url'])
            if self.id_regex is not None:
                post['id'] = post.get('id') and regex_groupd(post['id'], self.id_regex)['id'] or None
            post['source_id'] = self.source_id

            missing = [f for f in 'id url author pubdate body'.split()
                       if post.get(f) is None]
            missing and self.logger.error(
                u"Skiping post %d at %s which is missing %r!" % (
                    post_idx, response.url, missing))
            if not missing:
                yield post

    def parse_posts_grouped(self, response, hxs):
        if self.post_group_xpath is None:
            posts = hxs.xpath(self.post_container_xpath)
            posts or self.logger.warning(
                u'post_container_xpath got no posts at %s' % response.url)
        else:  # group nodes into posts using xpath
            posts, mixed = dict(), hxs.xpath(self.post_container_xpath)
            mixed or self.logger.warning(
                u'post_container_xpath got no posts at %s' % response.url)
            for postinfo in mixed:
                post_id, = postinfo.xpath(self.post_group_xpath) or [None]
                if post_id is None:
                    continue  # skip nodes that produce no identifiers
                post_id = post_id.xml()
                posts.setdefault(post_id, SelectorList()).append(postinfo)
            posts = list(posts.values())
            self.logger.debug(
                u'topic %s yields %s posts' % (response.url, len(posts)))

        for post_idx, post in enumerate(posts, 1):
            item = Item(id=u''.join(post.xpath(self.id_xpath).xml()))
            if self.post_link_xpath is None:
                item['url'] = response.url
            else:
                post_link = post.xpath(self.post_link_xpath).extract()
                try:
                    item['url'], = post_link
                except ValueError:
                    self.logger.warning(
                        u'Post %d at %s had the following links matched:\n%r'
                        % (post_idx, response.url, post_link))
            if self.title_xpath is not None:
                item['title'] = normalize_whitespace(u' '.join(post.xpath(self.title_xpath).render()))
            elif 'topic' in response.meta:
                item['title'] = response.meta['topic']
            item['author'] = normalize_whitespace(u' '.join(post.xpath(self.author_xpath).render()))
            item['pubdate'] = dt.datetime.now(BG_TZ)
            if self.pubdate_xpath is not None:
                item['pubdate'] = u' '.join(post.xpath(self.pubdate_xpath).render())
            item['body'] = u' '.join(post.xpath(self.body_xpath).sane_xml())
            yield [post_idx, item]

    def parse_posts_aligned(self, response, hxs):
        'Parse without post containers. all elements will be just zipped'
        ids = hxs.xpath(self.id_xpath).xml()
        titles = [None] * len(ids)
        if self.title_xpath is not None:
            titles = hxs.xpath(self.title_xpath).render() or [response.meta.get('topic')]
            # BUG: if only one title among many gets matched, it will get copied to the rest
            # BUG: if no title exists or non gets matched, it will use the topic link.text
            titles = titles * len(ids) if 1 == len(titles) else titles
        post_links = [response.url] * len(ids)
        if self.post_link_xpath is not None:
            post_links = hxs.xpath(self.post_link_xpath).extract()
        authors = hxs.xpath(self.author_xpath).render()
        pubdates = [dt.datetime.now(BG_TZ)] * len(ids)
        if self.pubdate_xpath is not None:
            pubdates = hxs.xpath(self.pubdate_xpath).render()
        bodies = hxs.xpath(self.body_xpath).sane_xml()
        posts = [
            dict(id=id, title=title, author=author, pubdate=pubdate, body=body,
                 post_link=post_link, post_idx=post_idx)
            for (post_idx, (id, title, author, pubdate, body, post_link))
            in enumerate(
                izip_longest(ids, titles, authors, pubdates, bodies, post_links,
                             fillvalue=Ellipsis),
                start=1)
        ]
        if any(Ellipsis in post.values() for post in posts):
            counts = {k: sum(1 for p in posts if p[k] is not Ellipsis)
                      for k in posts[0]}
            self.logger.error(
                u"Post elements at {} do not align:\n{}\n{}".format(
                    response.url, counts, pp.pformat(posts)))
            return
        for post_idx, post in enumerate(posts, 1):
            item = Item(id=post['id'], author=post['author'], body=post['body'],
                        pubdate=post['pubdate'], url=post['post_link'],
                        title=post['title'])
            yield [post_idx, item]


def get_pubdate(pubdate_str):
    if isinstance(pubdate_str, dt.date):
        return pubdate_str
    now = dt.datetime.now(BG_TZ)
    today = dt.date.today()
    pubdate_str = normalize_whitespace(pubdate_str).strip()
    if u'преди' in pubdate_str.lower():
        ago = int(re.search('\d+', pubdate_str).group())
        if u'дни' in pubdate_str.lower():
            pubdate_str = (today - dt.timedelta(days=ago)).isoformat()
        elif u'мин' in pubdate_str.lower():
            pubdate_str = (now - dt.timedelta(minutes=ago)).isoformat()
    elif u'днес' in pubdate_str.lower():
        pubdate_str = re.sub(u'днес', today.isoformat(), pubdate_str, flags=re.I | re.U)
    elif u'вчера' in pubdate_str.lower() or 'yesterday' in pubdate_str.lower():
        pubdate_str = re.sub(u'вчера|yesterday', (today - dt.timedelta(days=1)).isoformat(), pubdate_str, flags=re.I | re.U)
    else:
        bgmonts = [
            (u'ян(\w*|\W)', 'Jan'),
            (u'фев(\w*|\W)', 'Feb'),
            (u'мар(\w*|\W)', 'Mar'),
            (u'апр(\w*|\W)', 'Apr'),
            (u'май(\w*|\W)', 'May'),
            (u'юни(\w*|\W)', 'Jun'),
            (u'юли(\w*|\W)', 'Jul'),
            (u'авг(\w*|\W)', 'Aug'),
            (u'сеп(\w*|\W)', 'Sep'),
            (u'окт(\w*|\W)', 'Oct'),
            (u'ное(\w*|\W)', 'Nov'),
            (u'дек(\w*|\W)', 'Dec'),
        ]
        for bg, en in bgmonts:
            pubdate_str = re.sub(bg, en, pubdate_str, flags=re.I | re.U)
    pubdate_str = pubdate_str.encode('ascii', 'ignore').strip()
    # pubdate = dateutil.parser.parse(pubdate_str, dayfirst=True, fuzzy=True)
    pubdate = pubdate if pubdate.tzinfo else BG_TZ.localize(pubdate)
    pubdate = pubdate.astimezone(BG_TZ)
    assert now >= pubdate,\
        'parsed datetime in the future: %s > %s' % (pubdate, now)
    return pubdate
