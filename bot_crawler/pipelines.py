# -*- coding: utf-8 -*-

import psycopg2, psycopg2.extras
# , psycopg2.extension


class BotCrawlerPipeline(object):
    def __init__(self):
        self.conn = psycopg2.connect(
            host='localhost',
            user='bot_master',
            password='local_pass1',
            database='bot',
            cursor_factory=psycopg2.extras.DictCursor,
        )
        self.conn.autocommit = True
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):
        self.cursor.execute(
            "INSERT INTO bot_items.items (title, url, body, original_date, source_id, item_id_in_source)"
            " VALUES (%s, %s, %s, %s, %s, %s)",
            (item['title'], item['url'], item['body'], item['pubdate'], item['source_id'], item['id'])
            )
        return item
