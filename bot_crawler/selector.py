import operator as op
from collections import MutableSequence
import re
from xml.sax.saxutils import escape as xml_escape
import six
import lxml.html
from lxml.html.clean import Cleaner
from scrapy import selector


# The default Cleaner instance exported by lxm.html.clean doesn't clean <style>
clean_html = Cleaner(style=True).clean_html


def normalize_whitespace(string):
    return re.sub(ur'\s+', ' ', string, flags=re.U).strip()


class Selector(selector.Selector):

    def _extract_escaped(self):
        'Escapes back any text nodes or attributes that got unescaped by lxml'
        if isinstance(self.root, basestring):
            return xml_escape(self.root, {'"': '&quot;', '\'': '&apos;', })
        return self.extract()

    def _extract_encoded(self):
        'Like extract() but it expects only text that is encoded valid xml'
        assert isinstance(self.root, basestring), 'Selected Node is not text'
        return self.extract()

    def render(self):
        xml = self.sane_xml(abs_links=False)
        if not xml.strip():
            return u''
            # VNIMANIE VNIMANIE!
            # Due to BUG https://bugs.launchpad.net/lxml/+bug/761215
            # if an error about parsing an empty document occurs
            # the previous error will be shown instead!
        rendered = lxml.html.fromstring(xml).text_content().strip()
        return rendered

    def xml(self):
        return self._extract_escaped()

    # TODO: Method to extract with "encoded" expectation

    def _abs_links(self, xml):
        html = lxml.html.document_fromstring(xml)
        html.make_links_absolute(self.response.url)
        # The following has 3-cases
        # because lxml won't wrap single elements in the body
        # and the xpath will result will include whitespace text()
        xml = u'\n'.join(
                         elem if isinstance(elem, six.string_types)
                         else lxml.html.tostring(elem, encoding='unicode').strip()
                         if isinstance(elem, lxml.html.HtmlElement)
                         else lxml.html.tostring(elem, encoding='unicode')
                         for elem in html.body.xpath('node()')
                        )
        return xml

    def _sanitize_xml(self, xml):
        if xml.startswith('<!--') or not xml.strip():
            # Apparently, a single comment or space is not a valid html doc
            return u''
        return clean_html(xml)

    def sane_xml(self, encoded=False, abs_links=True):
        xml = self._extract_encoded() if encoded else self._extract_escaped()

        xml = self._sanitize_xml(xml)

        if xml and abs_links and self.response is not None:
            xml = self._abs_links(xml)

        return xml

    def xpath(self, *args, **kwargs):
        '''
            Wrap the method to return my SelectorList.
            Add back the response object.
            Is this prone to memory leaks? Maybe.
        '''
        return SelectorList(
            super(Selector, self).xpath(*args, **kwargs),
            self.response,
        )

    def css(self, *args, **kwargs):
        '''
            Wrap the method to return my SelectorList.
            Add back the response object.
            Is this prone to memory leaks? Maybe.
        '''
        return SelectorList(
            super(Selector, self).css(*args, **kwargs),
            self.response,
        )


class SelectorList(MutableSequence):

    @classmethod
    def _flat(cls, seq_of_seq):
        return cls(reduce(list.__add__, map(list, seq_of_seq), []))

    def map(self, mapper):
        return map(mapper, self)

    def __getattr__(self, name):
        if not callable(getattr(Selector, name)):
            return self.map(op.attrgetter(name))
        # Wrap once more for methods, cause that's how it's useful
        return lambda *a, **kw: self.map(op.methodcaller(name, *a, **kw))

    def xpath(self, *args, **kwargs):
        return self._flat(self.map(op.methodcaller('xpath', *args, **kwargs)))

    def css(self, *args, **kwargs):
        return self._flat(self.map(op.methodcaller('css', *args, **kwargs)))

    def re(self, *args, **kwargs):
        return self._flat(self.map(op.methodcaller('re', *args, **kwargs)))

    def __init__(self, seq=(), response=None):
        self._seq = list(seq[:])
        # Re-tie the response object, needed to get the url.
        # FIXME:memoryleaks?
        for elem in self._seq:
            if elem.response is None:
                elem.response = response
        self.response = response

    def __getitem__(self, key):
        return self._seq[key]

    def __len__(self):
        return len(self._seq)

    def count(self):
        return len(self._seq)

    def index(self, key, *args):
        return self._seq.index(key, *args)

    def __contains__(self, item):
        return item in self._seq

    def insert(self, *args, **kwargs):
        return self._seq.insert(*args, **kwargs)

    def __setitem__(self, *args, **kwargs):
        return self._seq.__setitem__(*args, **kwargs)

    def __delitem__(self, *args, **kwargs):
        return self._seq.__delitem__(*args, **kwargs)

    def append(self, *args, **kwargs):
        return self._seq.append(*args, **kwargs)

    def reverse(self, *args, **kwargs):
        return self._seq.reverse(*args, **kwargs)

    def extend(self, *args, **kwargs):
        return self._seq.extend(*args, **kwargs)

    def pop(self, *args, **kwargs):
        return self._seq.pop(*args, **kwargs)

    def remove(self, *args, **kwargs):
        return self._seq.remove(*args, **kwargs)

    def __iadd__(self, *args, **kwargs):
        return self._seq.__iadd__(*args, **kwargs)
