# -*- coding: utf-8 -*-

from ConfigParser import ConfigParser

config = ConfigParser()
config.read('settings.conf')
config_section = 'database'
secret_section = 'secret'

BOT_NAME = 'bot_crawler'

SPIDER_MODULES = ['bot_crawler.spiders']
NEWSPIDER_MODULE = 'bot_crawler.spiders'
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:37.1) Gecko/20100101 Firefox/37.0'
DOWNLOAD_DELAY = 0.5

# Configure maximum concurrent requests performed by Scrapy (default: 16)
# CONCURRENT_REQUESTS = 32
# The download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 16
# CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
COOKIES_ENABLED = False
LOG_LEVEL = 'INFO'

SPLASH_URL = 'http://0.0.0.0:8050'
# SPLASH_URL = config.get(secret_section, 'SPLASH_URL'),

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
# }
SPIDER_MIDDLEWARES = {
    'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
    'bot_crawler.crawlerdb_middleware.CrawlerDBStoreMiddleware': 200
}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy_splash.SplashCookiesMiddleware': 723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
}

# HTTPCACHE_ENABLED = True
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_DIR = 'httpcache'
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
CRAWLERDB_STORE_DBHOST = config.get(config_section, 'DATABASE_HOST')
CRAWLERDB_STORE_DBPORT = ''
CRAWLERDB_STORE_DATABASE = config.get(config_section, 'DATABASE_NAME')
CRAWLERDB_STORE_DBUSER = config.get(config_section, 'DATABASE_USER')
CRAWLERDB_STORE_DBPASSWORD = config.get(config_section, 'DATABASE_PASSWORD')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': CRAWLERDB_STORE_DATABASE,
        'USER': CRAWLERDB_STORE_DBUSER,
        'PASSWORD': CRAWLERDB_STORE_DBPASSWORD,
        'HOST': CRAWLERDB_STORE_DBHOST,
        'PORT': '',
    }
}

DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'

HTTPCACHE_STORAGE = 'scrapy_splash.SplashAwareFSCacheStorage'
