from contextlib import contextmanager
import time


@contextmanager
def timeit(spider, statkey):
    start = time.clock()
    yield
    spider.crawler.stats.inc_value(statkey, time.clock() - start)
