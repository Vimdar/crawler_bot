# -*- coding: utf-8 -*-

from scrapy.item import Item, Field
import scrapy


class Article(scrapy.Item):
    title = scrapy.Field()
    url = scrapy.Field()
    body = scrapy.Field()
    pubdate = scrapy.Field()
    author = Field()
    source_id = Field()
    id = Field()
    author_id = Field()


class Tweet(scrapy.Item):
    body = Field()
    author = Field()
    author_id = Field()
    pubdate = Field()
    url = Field()
    id_in_source = Field()
    shares = Field()
    source_name = Field()
    source_description = Field()
    source_url = Field()
    source_id_in_source = Field()
    source_country_id = Field()


class FacebookPost(scrapy.Item):
    parent_item_id = Field()
    source_id = Field()
    body = Field()
    author = Field()
    author_id = Field()
    original_date = Field()
    last_updated = Field()
    url = Field()
    item_id_in_source = Field()
    shares = Field()
    positive_votes = Field()
    comments = Field()
